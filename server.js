//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
/*app.get("/", function(req,res){
    res.send("Hola mundo nodeJS");
})
*/
app.get("/", function(req,res){
    res.sendFile(path.join(__dirname,'index.html'));
})

app.post("/", function(req,res){
   res.send("Recibimos su petición post");
})

app.get("/clientes/:idcliente/:nombre", function(req,res){
//  var json ="{'idcliente':233,'nombre':max}"
//res.send("Aquí tienes al cliente número:"+ req.params.idcliente);
var json ="{'idcliente':"+req.params.idcliente+",'nombre':"+req.params.nombre+"}"
res.send(json);
})

app.put("/", function(req,res){
   res.send("Recibimos su petición put cambiada");
})

app.delete("/", function(req,res){
   res.send("Recibimos su petición delete");
})

app.get("/v1/movimientos", function(req,res){
var json ="{'idcliente':"+req.params.idcliente+",'nombre':"+req.params.nombre+"}"
res.sendFile(path.join(__dirname,'movimientos1.json'));
})

var movimientosJSON = require('./movimientos2.json')
app.get("/v2/movimientos", function(req,res){
res.json(movimientosJSON);
})


app.get("/v2/movimientos/:id", function(req,res){
  console.log(req.params.id);
res.json(movimientosJSON[req.params.id-1]);
})

app.get('/v2/movimientosq', function(req,res){
  console.log(req.query);
  res.send("recibido")
})

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.post('/v2/movimientos', function(req,res){
  var nuevo = req.body;
  nuevo.id = movimientosJSON.length+1;
  movimientosJSON.push(nuevo);
  res.send("Movimiento dado de alta");
})
